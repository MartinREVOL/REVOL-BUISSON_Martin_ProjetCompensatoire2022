<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Déjà connecté</title>
</head>
<body>
    <h1>Déjà connecté</h1>
    <button onclick="goBack()">Retour</button>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    <p>Vous êtes déjà connecté. Pour revenir au menu de connexion, veuillez vous déconnecter en cliquant sur le bouton ci-dessous :</p>

    <form action="logout.php" method="post">
        <button type="submit">Se déconnecter</button>
    </form>
</body>
</html>
