<?php
include("db_connection.php");

if (!isset($_COOKIE['connection_good']) || $_COOKIE['connection_good'] !== 'true') {
    header('Location: login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Détails de l'Équipe</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="team-details">
        <h1>Détails de l'Équipe</h1>
        <div id="team-details-content">
        </div>
        <div class="team-members">
            <h2>Membres de l'Équipe</h2>
            <div id="team-members-list">
            </div>
        </div>
    </div>

    <script>
        function getTeamIdFromURL() {
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            return urlParams.get('team_id');
        }

        async function fetchTeamDetails() {
            try {
                const teamId = getTeamIdFromURL();

                if (!teamId) {
                    throw new Error('ID de l\'équipe non trouvé dans l\'URL.');
                }

                const teamResponse = await fetch(`https://www.balldontlie.io/api/v1/teams/${teamId}`);
                const teamData = await teamResponse.json();

                const teamDetailsContent = document.getElementById('team-details-content');
                teamDetailsContent.innerHTML = `
                    <p><strong>Abbréviation:</strong> ${teamData.abbreviation}</p>
                    <p><strong>Ville:</strong> ${teamData.city}</p>
                    <p><strong>Conférence:</strong> ${teamData.conference}</p>
                    <p><strong>Division:</strong> ${teamData.division}</p>
                    <p><strong>Nom complet:</strong> ${teamData.full_name}</p>
                    <p><strong>Nom:</strong> ${teamData.name}</p>
                `;

                const allTeamPlayers = await getAllTeamPlayers(teamId);

                const teamMembersList = document.getElementById('team-members-list');
                allTeamPlayers.forEach(member => {
                    const memberElement = document.createElement('div');
                    memberElement.className = 'team-member';

                    const playerDetailLink = document.createElement('a');
                    playerDetailLink.textContent = `${member.first_name} ${member.last_name}`;
                    playerDetailLink.href = `joueur_detail.php?id=${member.id}`;
                    memberElement.appendChild(playerDetailLink);

                    teamMembersList.appendChild(memberElement);
                });
            } catch (error) {
                console.error('Erreur lors de la récupération des détails de l\'équipe : ' + error);
            }
        }
        async function getAllTeamPlayers(teamId) {
            const allTeamPlayers = [];
            let page = 1;

            try {
                while (true) {
                    const response = await fetch(`https://www.balldontlie.io/api/v1/players?page=${page}`);
                    const data = await response.json();

                    const teamPlayers = data.data.filter(player => player.team.id === parseInt(teamId));

                    allTeamPlayers.push(...teamPlayers);

                    if (!data.meta.next_page) {
                        break;
                    }

                    page++;
                }

                return allTeamPlayers;
            } catch (error) {
                console.error('Erreur lors de la récupération des joueurs de l\'équipe : ' + error);
                return [];
            }
        }

        fetchTeamDetails();
    </script>
</body>
</html>
