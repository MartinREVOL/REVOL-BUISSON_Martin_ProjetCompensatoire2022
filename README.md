# Ynov Basket Projet
"Vous cherchez des informations sur vos joueurs de baskets préférés, une api
vous a été donné pour vous aider, mais les données telles qu'elles ne sont pas
très lisibles. ( http://www.balldontlie.io/ )

Pour pouvoir afficher ces données, les trier et les utiliser efficacement, vous
devrez réaliser un site web (local, pas besoin de l'héberger) qui affichera les données de l'API."

Nous allons donc se servir de XAMPP ([plus d'infos](https://www.apachefriends.org/fr/index.html)), pour notre site en localhost :
- lancez Apache et MySQL
- sur un navigateur, allez sur [phpmyadmin](http://localhost/phpmyadmin/) et importez-y [db.sql](./db.sql)
- rajoutez le projet dans le dossier `htdocs` de XAMPP
- allez sur votre navigateur et [lancez le projet en localhost](http://localhost/Projet%20Compensatoire/login.php)

![](https://media.tenor.com/MVibg1ll9EMAAAAd/miko-arrival.gif)
