<?php
include("db_connection.php");

if (!isset($_COOKIE['connection_good']) || $_COOKIE['connection_good'] !== 'true') {
    header('Location: login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Détails du Joueur</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>Détails du Joueur</h1>
    </header>

    <main>
        <div id="playerDetails">
        </div>
    </main>

    <footer>
    </footer>

    <script>
        function getPlayerIdFromURL() {
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            return urlParams.get('id');
        }

        async function fetchPlayerDetails() {
            const playerId = getPlayerIdFromURL();

            if (!playerId) {
                console.error('ID du joueur non spécifié dans l\'URL.');
                return;
            }

            try {
                const response = await fetch(`https://www.balldontlie.io/api/v1/players/${playerId}`);
                const playerData = await response.json();

                const playerDetailsContainer = document.getElementById('playerDetails');
                const playerDetails = document.createElement('div');

                playerDetails.innerHTML = `
                    <h2>Nom: ${playerData.last_name}</h2>
                    <h2>Prénom: ${playerData.first_name}</h2>
                    <h2>Position: ${playerData.position}</h2>
                    <h2>Taille: ${playerData.height_feet}' ${playerData.height_inches}"</h2>
                    <h2>Poids: ${playerData.weight_pounds} livres</h2>
                    <h2>Équipe: <a href="equipe_detail.php?team_id=${playerData.team.id}">${playerData.team.full_name}</a></h2>
                `;

                playerDetailsContainer.appendChild(playerDetails);
            } catch (error) {
                console.error('Erreur lors de la récupération des détails du joueur : ' + error);
            }
        }

        fetchPlayerDetails();
    </script>
</body>
</html>
