<?php
include("db_connection.php");

if (isset($_COOKIE['connection_good']) && $_COOKIE['connection_good'] === 'true') {
    header('Location: already_connected.php');
    exit;
}

$errorMessage = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);

    $email = $mysqli->real_escape_string($email);
    $password = $mysqli->real_escape_string($password);

    $sql = "SELECT * FROM users WHERE email = '$email'";
    $result = $mysqli->query($sql);

    if ($result && $result->num_rows === 1) {
        $row = $result->fetch_assoc();
        $hashedPassword = $row['pw'];

        if (password_verify($password, $hashedPassword)) {
            setcookie('connection_good', 'true', time() + 3600);
            header('Location: joueurs.php');
            exit;
        } else {
            $errorMessage = 'Mot de passe incorrect.';
        }
    } else {
        $errorMessage = 'Adresse e-mail non trouvée.';
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
</head>
<body>
    <h1>Connexion</h1>
    <?php if (!empty($errorMessage)) { ?>
        <p style="color: red;"><?php echo $errorMessage; ?></p>
    <?php } ?>
    <form action="login.php" method="post">
        <label for="email">E-mail :</label>
        <input type="email" name="email" required>
        <br>
        <label for="password">Mot de passe :</label>
        <input type="password" name="password" required>
        <br>
        <button type="submit">Se connecter</button>
    </form>
    <br>
    <a href="signIn.php">Pas encore inscrit ? S'inscrire</a>
</body>
</html>
