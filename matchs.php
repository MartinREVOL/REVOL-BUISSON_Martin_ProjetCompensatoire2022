<?php
include("db_connection.php");

if (!isset($_COOKIE['connection_good']) || $_COOKIE['connection_good'] !== 'true') {
    header('Location: login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des Matchs</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>YnovBasket</h1>
        <h2>Matchs</h2>
    </header>

    <a href="logout.php">Se déconnecter</a>
    
    <nav>
        <ul>
            <li><a href="joueurs.php">Joueurs</a></li>
            <li><a href="equipe.php">Équipes</a></li>
            <li><a href="matchs.php">Matchs</a></li>
        </ul>
    </nav>
    <div id="match-cards-container">
    </div>

    <script>
        function createMatchCard(match) {
            const card = document.createElement('div');
            card.className = 'match-card';

            const homeTeamAbbreviation = match.home_team.abbreviation;
            const visitorTeamAbbreviation = match.visitor_team.abbreviation;
            const matchDate = new Date(match.date);
            const formattedDate = `${matchDate.getDate()}/${matchDate.getMonth() + 1}/${matchDate.getFullYear()}`;

            card.innerHTML = `
                <p><strong>${homeTeamAbbreviation} VS ${visitorTeamAbbreviation}</strong></p>
                <p>Date: ${formattedDate}</p>
            `;

            card.addEventListener('click', () => {
                window.location.href = `match_detail.php?id=${match.id}`;
            });

            return card;
        }

        async function fetchMatches() {
            try {
                const response = await fetch('https://www.balldontlie.io/api/v1/games');
                const data = await response.json();

                const matchCardsContainer = document.getElementById('match-cards-container');

                data.data.forEach(match => {
                    const matchCard = createMatchCard(match);
                    matchCardsContainer.appendChild(matchCard);
                });
            } catch (error) {
                console.error('Erreur lors de la récupération des matchs depuis l\'API : ' + error);
            }
        }

        fetchMatches();
    </script>
</body>
</html>
