<?php
include("db_connection.php");

if (!isset($_COOKIE['connection_good']) || $_COOKIE['connection_good'] !== 'true') {
    header('Location: login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Détails du Match</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="match-details">
        <h1>Détails du Match</h1>
        <div id="match-details-content">
        </div>
    </div>

    <script>
        function getMatchIdFromURL() {
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            return urlParams.get('id');
        }

        async function fetchMatchDetails() {
            try {
                const matchId = getMatchIdFromURL();

                if (!matchId) {
                    throw new Error('ID du match non trouvé dans l\'URL.');
                }

                const response = await fetch(`https://www.balldontlie.io/api/v1/games/${matchId}`);
                const matchData = await response.json();

                const matchDetailsContent = document.getElementById('match-details-content');
                matchDetailsContent.innerHTML = `
                    <p><strong>Date:</strong> ${matchData.date}</p>
                    <p><strong>Score à domicile:</strong> ${matchData.home_team_score}</p>
                    <p><strong>Score à l'extérieur:</strong> ${matchData.visitor_team_score}</p>
                    <p><strong>Saison:</strong> ${matchData.season}</p>
                    <p><strong>Période:</strong> ${matchData.period}</p>
                    <p><strong>Statut:</strong> ${matchData.status}</p>
                    <p><strong>Série éliminatoire:</strong> ${matchData.postseason ? 'Oui' : 'Non'}</p>
                `;

                const homeTeamCityLink = document.createElement('a');
                homeTeamCityLink.textContent = matchData.home_team.city;
                homeTeamCityLink.href = `equipe_detail.php?team_id=${matchData.home_team.id}`;

                const visitorTeamCityLink = document.createElement('a');
                visitorTeamCityLink.textContent = matchData.visitor_team.city;
                visitorTeamCityLink.href = `equipe_detail.php?team_id=${matchData.visitor_team.id}`;

                matchDetailsContent.appendChild(document.createElement('br'));
                matchDetailsContent.appendChild(homeTeamCityLink);
                matchDetailsContent.appendChild(document.createTextNode(' VS '));
                matchDetailsContent.appendChild(visitorTeamCityLink);
            } catch (error) {
                console.error('Erreur lors de la récupération des détails du match : ' + error);
            }
        }

        fetchMatchDetails();
    </script>
</body>
</html>
