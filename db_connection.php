<?php
$host = "localhost";
$utilisateur = "root";
$mot_de_passe = "";
$base_de_donnees = "db";

$mysqli = new mysqli($host, $utilisateur, $mot_de_passe, $base_de_donnees);

if ($mysqli->connect_error) {
    die("Échec de la connexion à la base de données : " . $mysqli->connect_error);
}
?>
