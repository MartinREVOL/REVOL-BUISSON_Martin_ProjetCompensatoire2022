<?php
include("db_connection.php");

if (isset($_COOKIE['connection_good']) && $_COOKIE['connection_good'] === 'true') {
    header('Location: already_connected.php');
    exit;
}

$errorMessage = "";
$successMessage = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $email = $mysqli->real_escape_string($email);

    $checkEmailSql = "SELECT * FROM users WHERE email = '$email'";
    $checkEmailResult = $mysqli->query($checkEmailSql);

    if ($checkEmailResult->num_rows > 0) {
        $errorMessage = 'Cette adresse e-mail est déjà utilisée.';
    } else {
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $insertSql = "INSERT INTO users (email, pw) VALUES ('$email', '$hashedPassword')";
        if ($mysqli->query($insertSql) === TRUE) {
            $successMessage = 'Inscription réussie. Vous pouvez maintenant vous connecter.';
        } else {
            $errorMessage = 'Erreur lors de l\'inscription : ' . $mysqli->error;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
</head>
<body>
    <h1>Inscription</h1>
    <?php if (!empty($errorMessage)) { ?>
        <p style="color: red;"><?php echo $errorMessage; ?></p>
    <?php } ?>
    <?php if (!empty($successMessage)) { ?>
        <p style="color: green;"><?php echo $successMessage; ?></p>
    <?php } ?>
    <form action="signIn.php" method="post">
        <label for="email">E-mail :</label>
        <input type="email" name="email" required>
        <br>
        <label for="password">Mot de passe :</label>
        <input type="password" name="password" required>
        <br>
        <button type="submit">S'inscrire</button>
    </form>
    <br>
    <a href="login.php">Déjà inscrit ? Se connecter</a>
</body>
</html>
