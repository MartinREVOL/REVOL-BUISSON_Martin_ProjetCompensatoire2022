<?php
include("db_connection.php");

if (!isset($_COOKIE['connection_good']) || $_COOKIE['connection_good'] !== 'true') {
    header('Location: login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>YnovBasket</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <header>
        <h1>YnovBasket</h1>
        <h2>Équipes</h2>
    </header>

    <a href="logout.php">Se déconnecter</a>
    
    <nav>
        <ul>
            <li><a href="joueurs.php">Joueurs</a></li>
            <li><a href="equipe.php">Équipes</a></li>
            <li><a href="matchs.php">Matchs</a></li>
        </ul>
    </nav>

    <div id="cardsContainer">
    </div>

    <script>
        function createTeamCard(team) {
            const card = document.createElement('div');
            card.className = 'card';

            const fullNameElement = document.createElement('h2');
            fullNameElement.textContent = `Nom complet: ${team.full_name}`;
            card.appendChild(fullNameElement);

            const teamDetailLink = document.createElement('a');
            teamDetailLink.href = `equipe_detail.php?team_id=${team.id}`;
            teamDetailLink.textContent = 'Détails de l\'équipe';
            card.appendChild(teamDetailLink);

            return card;
        }

        async function fetchTeams() {
            try {
                const response = await fetch('https://www.balldontlie.io/api/v1/teams');
                const data = await response.json();

                const cardsContainer = document.getElementById('cardsContainer');

                data.data.forEach(team => {
                    const teamCard = createTeamCard(team);
                    cardsContainer.appendChild(teamCard);
                });
            } catch (error) {
                console.error('Erreur lors de la récupération des données de l\'API : ' + error);
            }
        }

        fetchTeams();
    </script>

    <footer>
    </footer>
</body>
</html>

