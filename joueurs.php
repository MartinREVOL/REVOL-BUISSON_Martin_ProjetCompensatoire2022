<?php
include("db_connection.php");

if (!isset($_COOKIE['connection_good']) || $_COOKIE['connection_good'] !== 'true') {
    header('Location: login.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>YnovBasket</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <header>
        <h1>YnovBasket</h1>
        <h2>Joueurs</h2>
    </header>

    <a href="logout.php">Se déconnecter</a>
    
    <nav>
        <ul>
            <li><a href="joueurs.php">Joueurs</a></li>
            <li><a href="equipe.php">Équipes</a></li>
            <li><a href="matchs.php">Matchs</a></li>
        </ul>
    </nav>
    <div id="cardsContainer">
    </div>

    <script>
        function createPlayerCard(player) {
            const card = document.createElement('a');
            card.href = `joueur_detail.php?id=${player.id}`;
            card.className = 'card';

            const nameElement = document.createElement('h2');
            nameElement.textContent = `Nom: ${player.last_name}`;
            card.appendChild(nameElement);

            const firstNameElement = document.createElement('h2');
            firstNameElement.textContent = `Prénom: ${player.first_name}`;
            card.appendChild(firstNameElement);

            return card;
        }

        async function fetchPlayers(page = 1) {
            try {
                const response = await fetch(`https://www.balldontlie.io/api/v1/players?page=${page}`);
                const data = await response.json();

                const cardsContainer = document.getElementById('cardsContainer');

                data.data.forEach(player => {
                    const playerCard = createPlayerCard(player);
                    cardsContainer.appendChild(playerCard);
                });

                if (data.meta.next_page) {
                    await fetchPlayers(data.meta.next_page);
                }
            } catch (error) {
                console.error('Erreur lors de la récupération des données de l\'API : ' + error);
            }
        }

        fetchPlayers();
    </script>
</body>
</html>
